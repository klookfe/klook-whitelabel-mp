# 小程序white label
### 项目背景
以小程序的web-view[页面承载](https://uniapp.dcloud.io/component/web-view)把我们的White label web嵌套进小程序,实现在小程序内打开white label。

#### 本地开发
***本地运行本项目后需同时在本地运行[whitelabel](https://bitbucket.org/klook/klook-whitelabel-web/src/master/)项目***

---
#### ENV
```
js
node v8.11.3
npm 5.6.0
```

## [dev && build](https://uniapp.dcloud.io/quickstart?id=%e8%bf%90%e8%a1%8c%e5%b9%b6%e5%8f%91%e5%b8%83uni-app)

```
npm install
```

```
npm run dev:%PLATFORM%
npm run build:%PLATFORM%
```

`%PLATFORM%` 可取值如下（webview只适用于小程序）：

| 值 | 平台 |
| --- | --- |
| ~~h5~~ | ~~H5~~ |
| mp-alipay | 支付宝小程序 |
| mp-baidu | 百度小程序 |
| mp-weixin | 微信小程序 |
| mp-toutiao | 头条小程序 |

---
### webview遇到的问题
webview与网页通信可以通过postMessage方法实现网页向小程序发送消息，但是目前仅发现支付宝小程序是实时通信，其他小程序在网页向应用 postMessage 时，会在特定时机（后退、组件销毁、分享）触发并收到消息([文档地址](https://uniapp.dcloud.io/component/web-view))。因此无法通过postMessage实现交互。

目前的解决方案是通过webview和小程序之间互相跳转并携带参数。([参考方案](https://www.cnblogs.com/zhuanzhuanfe/p/9754482.html))