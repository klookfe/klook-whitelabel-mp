export function postMessage(webContext, message, res) {
  webContext.postMessage({type: 'apiCall', api: message.detail.apiName, result: res, serialId: message.detail.serialId});
}