import BasicHandler from './BasicHandler';

export default function AuthCodeHandler(webContext) {
  return new BasicHandler(webContext, 'getAuthCode', my.getAuthCode);
}
