import { postMessage } from '../util';

export default function BasicHandler(webContext, apiName, apiMedhod, isPrivate) {
  this.webContext = webContext;
  this.apiName = apiName;
  this.apiMethod = apiMedhod;
  this.isPrivate = isPrivate;
  this.handleMessage = (message) => {
    if (!this.apiName || !this.webContext) {
      return;
    }
    if (this.isPrivate) {
      my.call(this.apiName, {
        ...message.detail.params,
        success: (res) => {
          postMessage(this.webContext, message, {success: true, data: res});
        },
        fail: (res) => {
          postMessage(this.webContext, message, {success: false, data: res});
        }
      });
    } else {
      if (!this.apiMethod || !my.canIUse(this.apiName)) {
        postMessage(this.webContext, message, {success: false, data: {"errorMsg" : "my." + this.apiName + " is not supported"}});
      } else {
        this.apiMethod({
          ...message.detail.params,
          success: (res) => {
            postMessage(this.webContext, message, {success: true, data: res});
          },
          fail: (res) => {
            postMessage(this.webContext, message, {success: false, data: res});
          }
        });
      }
    }
  }
}