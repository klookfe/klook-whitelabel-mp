import BasicHandler from './BasicHandler';

export default function SiteInfoHandler(webContext) {
  var handler = new BasicHandler(webContext, 'getSiteInfo', my.getSiteInfo, true);
  return handler;
}