import BasicHandler from './BasicHandler';

export default function SystemInfoHandler(webContext) {
  return new BasicHandler(webContext, 'getSystemInfo', my.getSystemInfo);
}