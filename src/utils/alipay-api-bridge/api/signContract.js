import BasicHandler from './BasicHandler';

export default function SignContractHandler(webContext) {
  var handler = new BasicHandler(webContext, 'signContract', my.signContract, true);
  return handler;
}