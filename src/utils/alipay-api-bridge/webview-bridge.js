import MessageDispatcher from "./message-dispacther"
import AuthCodeHandler from './api/authCode'
import SystemInfoHandler from './api/systemInfo'
import SignContractHandler from './api/signContract'
import SiteInfoHandler from './api/siteInfo'

export default function WebViewBridge(webviewId) {
  this.webViewContext = my.createWebViewContext(webviewId);
  this.messageDispatcher = new MessageDispatcher(this.webViewContext);
  this.messageDispatcher.registerHandler(new AuthCodeHandler(this.webViewContext));
  this.messageDispatcher.registerHandler(new SystemInfoHandler(this.webViewContext));
  this.messageDispatcher.registerHandler(new SignContractHandler(this.webViewContext));
  this.messageDispatcher.registerHandler(new SiteInfoHandler(this.webViewContext));

  this.handleMessage = (message) => {
    return this.messageDispatcher.handleMessage(message);
  }
}
