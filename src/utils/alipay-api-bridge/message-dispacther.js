export default function MessageDispatcher() {
  this.registerHandler = (handler) => {
    if (!handler || !handler.apiName) {
      return;
    }
    if (!this.messageHandlers) {
      this.messageHandlers = {};
    }
    this.messageHandlers[handler.apiName] = handler;
  }

  this.handleMessage = (message) => {
    let apiName = message.detail.api;
    let type = message.detail.type;
    if (apiName && type === 'apiCall') {
      let handler = this.messageHandlers[apiName];
      if (handler) {
        handler.handleMessage(message);
        return true;
      }
    }
    return false;
  }
}
