interface SystemInfo {
    model: string;
    pixelRatio: Number;
    windowWidth: Number;
    windowHeight: Number;
    language: 'zh-Hans' | 'en' | 'zh-Hant' | 'zh-HK'; // zh-Hans（简体中文）、en（English）、zh-Hant（繁体中文（台湾））、zh-HK（繁体中文（香港））。
    version: string;
    storage: string;
    currentBattery: string;
    system: string;
    platform: string;
    titleBarHeight: Number;
    statusBarHeight: Number;
    screenWidth: Number;
    screenHeight: Number;
    brand: string;
    fontSizeSetting: Number;
    app: string;
}

namespace SiteInfo {
    export type siteName = 'EASYPASIS' | 'DANA' | 'GCASH' | 'TRUEMONEY' | 'TNG' | 'BKASH' | 'KAKAOPAY' | 'ALIPAY_CN' | 'ALIPAY_HK'
}

export { SystemInfo, SiteInfo }